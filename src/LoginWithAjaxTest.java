import org.junit.Test;
import org.openqa.selenium.By;

public class LoginWithAjaxTest extends WebAppTestBase {

    @Test
    public void testForgetPasswordCase() throws Exception {
        gotoUrl(TestConstants.LOGIN_WITH_AJAX_URL);
        prepareJsErrorCollector();

        waitAndClickElementLocated(By.id("lwa_wp-submit"));
        waitAndClickElementLocated(By.id("LoginWithAjax_Status"));
        waitAndClickElementLocated(By.id("lwa_user_remember"));

        assertNoJsErrorObserved();
    }
}
