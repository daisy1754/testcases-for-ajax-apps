import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;

import static org.junit.Assert.assertEquals;

public class EcTest extends WebAppTestBase {

    @Test
    public void testTransactionLogIncreaseByOne() throws Exception {
        int numOfLog = ServerUtils.getNumberOfLogItems(TestConstants.EC_URL);
        gotoUrl(TestConstants.EC_URL);

        waitAndClickElementLocated(By.id("addcart"));
        // Try double clicking (should be handled properly)
        tryClickIfPossible(By.id("addcart"));

        assertEquals("Transaction log must increase by exactly one",
                numOfLog + 1, ServerUtils.getNumberOfLogItems(TestConstants.EC_URL));
    }

    private void tryClickIfPossible(By by) {
        try {
            waitAndClickElementLocated(by);
        } catch (TimeoutException e) {
            // Expected exception.
        }
    }
}
