import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static com.thoughtworks.selenium.SeleneseTestCase.assertEquals;
import static junit.framework.Assert.assertTrue;

public class TwentyTwentyMarketTest extends WebAppTestBase {

    @Test
    public void testLoginSuccess() {
        gotoUrl(TestConstants.TWENTY_TWENTY_URL);
        driver.findElement(By.id("user")).sendKeys("yuta");
        driver.findElement(By.id("pass")).sendKeys("maezawa");
        waitAndClickElementLocated(By.id("loginbtn"));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("response")));
        WebElement responseText = driver.findElement(By.className("response"));
        assertEquals("Hello, yuta", responseText.getText());
    }

    @Test
    public void testLoginFailure() {
        gotoUrl(TestConstants.TWENTY_TWENTY_URL);
        driver.findElement(By.id("user")).sendKeys("yuta");
        driver.findElement(By.id("pass")).sendKeys("invalid");
        waitAndClickElementLocated(By.id("loginbtn"));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("response")));
        WebElement responseText = driver.findElement(By.className("response"));
        assertTrue(responseText.getText().contains("Invalid"));
    }
}
