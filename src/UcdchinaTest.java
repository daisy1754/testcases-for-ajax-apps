import com.google.common.base.Predicate;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class UcdchinaTest extends WebAppTestBase {
    @Override
    public void setupBrowser() throws Exception {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("webdriver.load.strategy", "unstable");
        driver = new FirefoxDriver(profile);
        wait = new WebDriverWait(driver, TIMEOUT_SEC);
    }

    @Test
    public void searchWithBlankTextShouldHandledInPage() {
        final String startUrl = TestConstants.UCD_CHINA_URL;
        try {
            driver.manage().timeouts().pageLoadTimeout(2, TimeUnit.SECONDS);
            gotoUrl(startUrl);
        } catch (TimeoutException e){
            // Expected exception, when timeout occurs, move to next operations.
        }
        WebElement searchForm = wait.until(
                ExpectedConditions.presenceOfElementLocated(By.id("searchBox")));
        searchForm.submit();
        try {
            wait.until(new Predicate<WebDriver>() {
                @Override
                public boolean apply(WebDriver webDriver) {
                    return !webDriver.getCurrentUrl().equals(startUrl);
                }
            });
            fail("We shouldn't allow page transition, but current url is " + driver.getCurrentUrl());
        } catch (TimeoutException e) {
            // Expected exception.
        }
    }
}
