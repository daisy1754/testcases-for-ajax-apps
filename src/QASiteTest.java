import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import static com.thoughtworks.selenium.SeleneseTestCase.assertEquals;
import static junit.framework.Assert.fail;

public class QASiteTest extends WebAppTestBase {

    @Test
    public void testLoginSuccess() {
        gotoUrl(TestConstants.QA_URL);
        driver.findElement(By.id("username")).sendKeys("yuta");
        driver.findElement(By.id("password")).sendKeys("maezawacd ");
        waitAndClickElementLocated(By.id("login"));
        assertEquals("Yuta Maezawa", driver.findElement(By.id("fullname")).getText());
        // We should be able to find logout link
        driver.findElement(By.id("logout"));
    }

    @Test
    public void shouldNotAbleToClickGoodButtonBeforeLoggedin() {
        gotoUrl(TestConstants.QA_URL);
        WebElement goodButton = driver.findElement(By.id("good"));
        try {
            goodButton.click();
            fail("Click must be failed before logged in.");
        } catch (WebDriverException e) {
            // Excepted exception
        }
    }

    @Test
    public void shouldNotHandleMultipleLogin() {
        gotoUrl(TestConstants.QA_URL);
        driver.findElement(By.id("username")).sendKeys("yuta");
        driver.findElement(By.id("password")).sendKeys("maezawa");
        waitAndClickElementLocated(By.id("login"));
        try {
            waitAndClickElementLocated(By.id("login"));
            fail("Login button should be operated only once if params are valid.");
        } catch (WebDriverException e) {
            // Excepted exception
        }
    }
}
